package parking;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import parking.builder.Director;
import parking.builder.ParkingModelBuilder;
import parking.front.ParkingView;
import parking.loader.Loader;
import parking.model.Model;

public class Main {

	public static void main(String[] args) {

		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(new File(args[0])));
		} catch (IOException e) {
			System.err.println(String.format("Error reading file %s", args[0]));
		}

		Model model = createModel(properties);
		ParkingView view = new ParkingView(model);

		view.render();
	}

	private static Model createModel(Properties properties) {

		String jsonPath = properties.getProperty("JSON_PATH");

		Loader classLoader = new Loader(jsonPath);
		Director director = new Director(properties, classLoader.getInstances());

		ParkingModelBuilder builder = new ParkingModelBuilder();
		director.buildParking(builder);
		return builder.getModel();
	}
}
