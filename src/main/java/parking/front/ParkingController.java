package parking.front;

import parking.model.Model;
import parking.model.Observer;

public class ParkingController implements Observer {

	private Model model;

	public ParkingController(ParkingView view) {
		this.model = view.getModel();
	}

	public void handleProcessLicence(String licenseNumber) {
		this.model.process(licenseNumber);
	}
	
	public void startImageDetector() {
		this.model.startFileDetection();
	}
	
	public void stopImageDetector() {
		this.model.stopFileDetection();
	}

	@Override
	public void update() {
		System.out.println("ParkingController update");
	}

}
