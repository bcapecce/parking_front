package parking.front;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import parking.model.Model;
import parking.model.Observer;

public class ParkingView extends JFrame implements Observer {
	private static final long serialVersionUID = 1L;
	private JPanel contentPanel;
	private JButton btnArrive;
	private JButton btnStartFileDetection;
	private JButton btnStopFileDetection;
	private JTextField txtPatent;
	private JLabel lblprocessed;
	private JLabel lblError;

	private Model model;
	private ParkingController controller;

	public ParkingView(Model model) {
		super();
		this.model = model;
	}

	public void render() {
		this.controller = new ParkingController(this);
		this.model.attach(controller);
		this.model.attach(this);
		renderView();
	}
	
	public static void setTimeout(Runnable runnable, int delay){
	    new Thread(() -> {
	        try {
	            Thread.sleep(delay);
	            runnable.run();
	        }
	        catch (Exception e){
	            System.err.println(e);
	        }
	    }).start();
	}

	private void renderView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 200, 400, 250);
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 440, 250);
		contentPanel.add(panel);
		panel.setLayout(null);

		JLabel lblPatent = new JLabel("Enter car patent");
		lblPatent.setBounds(15, 10, 260, 15);
		panel.add(lblPatent);
		txtPatent = new JTextField();
		txtPatent.setBounds(10, 30, 260, 20);
		txtPatent.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (txtPatent.getText().length() >= 20) {
					JOptionPane.showMessageDialog(null,
							"Car patent can not have more than 20 characters.");
					e.consume();
				}
			}
		});
		panel.add(txtPatent);
		txtPatent.setColumns(10);

		btnArrive = new JButton("Register");
		btnArrive.setBounds(6, 60, 120, 25);
		btnArrive.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (txtPatent.getText().length() == 0) {
					JOptionPane.showMessageDialog(null, "Please, enter a car patent.");
				} else {
					try {
						controller.handleProcessLicence(txtPatent.getText());	
					}catch (Exception exception) {
						lblError.setText(exception.getMessage());
						lblError.setVisible(true);
						setTimeout(() -> lblError.setVisible(false), 4000);
						
					}
					txtPatent.setText("");
				}
			}
		});
		;
		panel.add(btnArrive);
		
		lblprocessed = new JLabel("Car registered correctly.");
		lblprocessed.setBounds(18, 150, 200, 25);
		lblprocessed.setVisible(false);
        panel.add(lblprocessed);
        
        lblError = new JLabel("");
		lblError.setBounds(18, 150, 300, 25);
		lblError.setVisible(false);
        panel.add(lblError);
	
		
		this.setVisible(true);
	}

	public Model getModel() {
		return model;
	}

	@Override
	public void update() {
		lblprocessed.setVisible(true);
		setTimeout(() -> lblprocessed.setVisible(false), 4000);
	}

}
